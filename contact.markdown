---
title: Contact
---

<p>Needing someone to work done? Contact me at <a href="mailto:contact@extractmethod.com">contact@extractmethod.com</a></p>

Otherwise, get me here:
<ul>
<li>twitter: <a href="http://twitter.com/bishboria">bishboria</a></li>
<li>ADN: <a href="http://app.net/bishboria">bishboria</a></li>
<li>email: <a href="mailto:bishboria@gmail.com">bishboria@gmail.com</a></li>
</ul>
