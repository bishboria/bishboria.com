---
title: About @bishboria
---
My day job is a `Ruby contractor`_, based in London. At night I'm into functional programming - using `Haskell`_, `Idris`_, `Agda`_, and `Coq`_ - and learning about Type Theory.

.. _Ruby contractor: http://extractmethod.com
.. _Haskell: http://haskell.org
.. _Idris: http://idris-lang.org
.. _Agda: http://wiki.portal.chalmers.se/agda/pmwiki.php
.. _Coq: http://coq.inria.fr/

This site uses `hakyll`_ to render content. It also uses `Computer
Modern Unicode`_ fonts configured for `the web`_.

.. _hakyll: http://jaspervdj.be/hakyll/
.. _Computer Modern Unicode: http://cm-unicode.sourceforge.net/
.. _the web: http://checkmyworking.com/cm-web-fonts/

The posts for the site are in `LaTeX`_, and the source
code is on `Bitbucket`_.

.. _LaTeX: http://www.latex-project.org/
.. _Git: http://git-scm.com/
.. _Bitbucket: https://bitbucket.org/bishboria/bishboria.com
